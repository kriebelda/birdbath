import unittest
from time import sleep, time
from utils.birdbath.Perch import Perch, RetryConfig
from utils.birdbath.ErrorHandling import PerchGenericError, PerchPassThroughError


class Birdbath_Connection:
    def __init__(self, name, pw):
        self.id = time()
        self.healthy = True
        self.name = name
        self.pw = pw


class RetryCount:
    def __init__(self):
        self.start_time = None
        self.last_time = None
        self.retries = -1

    def custom_retry_calc(self):
        self.retries += 1
        self.start_time = self.start_time if self.start_time else time()
        self.last_time = time()
        raise ValueError("testing retry")


def connect():
    return Birdbath_Connection("test", "testing")


def connect_fail():
    raise ValueError("connect fail")


def connect_fail_pass_through():
    try:
        raise ValueError("connect_fail_pass_through fail")
    except ValueError:
        raise PerchPassThroughError("BasinPassThroughError")


def disconnect(obj):
    return True


def disconnect_fail(obj):
    raise ValueError("disconnect fail")

non_function = None


class PerchTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_perch_creation(self):
        perch = Perch(connect)
        self.assertIsNone(perch._perch, "Perch created the obj before is was supposed to")
        perch.stand()
        self.assertIsNotNone(perch._perch, "Perch should have and object, but it does not.")

    def test_perch_get_obj(self):
        perch = Perch(connect).stand()
        self.assertIsInstance(perch.get(), Birdbath_Connection)

    def test_perch_refresh_seconds_requires_disposer(self):
        self.assertRaises(ValueError, lambda: Perch(connect, refresh_seconds=2))

    def test_perch_ignore_disposer_refresh(self):
        perch = Perch(connect, refresh_seconds=2, ignore_disposer=True)
        self.assertIsInstance(perch, Perch)

    def test_perch_refresh_seconds(self):
        perch = Perch(connect,  disposer=disconnect, refresh_seconds=1).stand()
        obj = perch.get()
        ex_obj = obj.id
        sleep(1.1)
        obj = perch.get()
        self.assertGreater(obj.id, ex_obj)
        perch.stop_refresh()

    def test_perch_start_stop_refresh(self):
        perch = Perch(connect, disposer=disconnect, refresh_seconds=1).stand()
        obj = perch.get().id
        # obj_id = obj.id
        perch.stop_refresh()
        sleep(1.5)
        # obj = perch.get().id
        # obj_id2 = obj.id
        self.assertEqual(obj, perch.get().id, "The stop_refresh did not stop the refresh from happening")
        perch.start_refresh()
        sleep(1.5)
        # obj = perch.get().id
        # obj_id2 = obj.id
        self.assertNotEqual(obj, perch.get().id, "The start_refresh did not start the refreshing")
        perch.stop_refresh()

    def test_perch_replace(self):
        perch = Perch(connect).stand()
        obj = perch.get().id
        perch.replace()
        self.assertNotEqual(obj, perch.get().id)

    def test_perch_refresh_with_refresh_as_false(self):
        perch = Perch(connect, disposer=disconnect, refresh_seconds=1).stand()
        perch.stop_refresh()
        self.assertFalse(perch.refresh())

    def test_perch_GenericError_with_create(self):
        retry_config = RetryConfig(retry=False)
        perch = Perch(connect_fail, retry_config=retry_config)
        self.assertRaises(PerchGenericError, lambda: perch._create())

    def test_perch_GenericError_with_dispose(self):
        retry_config = RetryConfig(retry=False)
        perch = Perch(connect, disposer=disconnect_fail, retry_config=retry_config).stand()
        self.assertRaises(PerchGenericError, lambda: perch._dispose())

    def test_perch_PassThroughError(self):
        perch = Perch(connect_fail_pass_through)
        self.assertRaises(ValueError, lambda: perch.stand())

if __name__ == '__main__':
    unittest.main()
