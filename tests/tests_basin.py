import unittest
from time import sleep, time
from utils.birdbath.Basin import Basin, RetryConfig
from utils.birdbath.ErrorHandling import BasinGenericError, BasinOutOfRange, BasinPassThroughError
from utils.birdbath.Water import Water


class Birdbath_Connection:
    def __init__(self, name, pw):
        self.id = 1
        self.healthy = True
        self.name = name
        self.pw = pw


class RetryCount:
    def __init__(self):
        self.start_time = None
        self.last_time = None
        self.retries = -1

    def custom_retry_calc(self):
        self.retries += 1
        self.start_time = self.start_time if self.start_time else time()
        self.last_time = time()
        raise ValueError("testing retry")


def connect():
    return Birdbath_Connection("test", "testing")


def connect_fail():
    raise ValueError("connect fail")


def connect_fail_pass_through():
    try:
        raise ValueError("connect_fail_pass_through fail")
    except ValueError:
        raise BasinPassThroughError("BasinPassThroughError")



def disconnect(obj):
    return True


def disconnect_fail(obj):
    raise ValueError("disconnect fail")

non_function = None


class BasinTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_basin_creation(self):
        pool = Basin(connect, 2).fill()
        self.assertEqual(2, pool._basin.qsize(), "connection pool was not created to the proper size")

    def test_get_obj(self):
        pool = Basin(connect, 2).fill()
        obj = pool.get()
        self.assertEqual(1, pool._basin.qsize(), "connection pool is not the proper size after get")
        self.assertIsInstance(obj, Water, "the pool did not return the proper object")

    def test_put_obj(self):
        pool = Basin(connect, 2).fill()
        obj = pool.get()
        self.assertEqual(1, pool._basin.qsize(), "connection pool is not the proper size after get")
        pool.put(obj)
        self.assertEqual(2, pool._basin.qsize(), "connection pool is not the proper size after put")

    def test_refresh_seconds_requires_disposer(self):
        self.assertRaises(ValueError, lambda: Basin(connect, 2, refresh_seconds=2))

    def test_grow_requires_disposer(self):
        self.assertRaises(ValueError, lambda: Basin(connect, 2, grow=True))

    def test_refresh_seconds_on_put(self):
        pool = Basin(connect, 2, disposer=disconnect, refresh_seconds=2).fill()
        obj_1 = pool.get()
        obj_2 = pool.get()
        obj_1_refresh = obj_1.basin_expiration
        obj_2_refresh = obj_2.basin_expiration
        sleep(2)
        pool.put(obj_1).put(obj_2)
        obj_1 = pool.get()
        obj_2 = pool.get()
        self.assertGreater(obj_1.basin_expiration, obj_1_refresh, "A new obj was not created on refresh for a put")
        self.assertGreater(obj_2.basin_expiration, obj_2_refresh, "A new obj was not created on refresh for a put")
        pool.stop_refresh()

    def test_refresh_seconds_on_get(self):
        pool = Basin(connect, 2, disposer=disconnect, refresh_seconds=2).fill()
        obj_1 = pool.get()
        obj_2 = pool.get()
        obj_1_refresh = obj_1.basin_expiration
        obj_2_refresh = obj_2.basin_expiration
        pool.put(obj_1).put(obj_2)
        sleep(2)
        obj_1 = pool.get()
        obj_2 = pool.get()
        self.assertGreater(obj_1.basin_expiration, obj_1_refresh, "A new obj was not created on refresh for a put")
        self.assertGreater(obj_2.basin_expiration, obj_2_refresh, "A new obj was not created on refresh for a put")
        pool.stop_refresh()

    def test_refresh_seconds(self):
        pool = Basin(connect, 1, disposer=disconnect, refresh_seconds=1).fill()
        obj = pool.get()
        ex_obj = obj.basin_expiration
        pool.put(obj)
        sleep(1.1)
        obj = pool.get()
        self.assertGreater(obj.basin_expiration, ex_obj)
        pool.stop_refresh()

    def test_grow(self):
        pool = Basin(connect, 1, max_size=3, disposer=disconnect, grow=True).fill()
        obj_1 = pool.get()
        self.assertEqual(1, pool._current_number_of_objects)
        obj_2 = pool.get()
        self.assertEqual(2, pool._current_number_of_objects)
        obj_3 = pool.get()
        self.assertEqual(3, pool._current_number_of_objects)
        pool.put(obj_1)
        self.assertEqual(3, pool._current_number_of_objects)
        pool.put(obj_2)
        self.assertEqual(2, pool._current_number_of_objects)
        pool.put(obj_3)
        self.assertEqual(1, pool._current_number_of_objects)
        self.assertEqual(1, pool._basin.qsize(), "The pool size did not return to min_size after growing")

    def test_set_disposer(self):
        pool = Basin(connect, 1)
        self.assertIsNone(pool._disposer, 'Disposer should be None by default, but is not')
        pool.set_disposer(disconnect)
        self.assertEqual(pool._disposer.func_name, "disconnect", "The disposer was not set properly")

    def test_verify_function_type(self):
        self.assertEqual(Basin._verify_function_type(connect).func_name, "connect")

    def test_verify_function_type_non_function(self):
        self.assertRaises(ValueError, lambda: Basin._verify_function_type(non_function))

    def test_empty(self):
        pool = Basin(connect, 3, disposer=disconnect).fill()
        self.assertEqual(3, pool._basin.qsize())
        pool.empty()
        self.assertEqual(0, pool._basin.qsize())

    def test_empty_no_disposer(self):
        self.assertRaises(ValueError, lambda: Basin(connect, 2).fill().empty())

    def test_empty_with_put(self):
        pool = Basin(connect, 3, disposer=disconnect).fill()
        obj = pool.get()
        self.assertEqual(2, pool._basin.qsize())
        pool.empty()
        self.assertEqual(0, pool._basin.qsize())
        pool.put(obj)
        self.assertEqual(0, pool._basin.qsize())

    def test_empty_with_grow(self):
        pool = Basin(connect, 1, grow=True, max_size=3, disposer=disconnect).fill()
        obj_1 = pool.get()
        obj_2 = pool.get()
        obj_3 = pool.get()
        self.assertEqual(3, pool._current_number_of_objects)
        pool.empty()
        self.assertEqual(3, pool._current_number_of_objects)
        self.assertEqual(0, pool._basin.qsize())
        pool.put(obj_1).put(obj_2)
        self.assertEqual(1, pool._current_number_of_objects)
        self.assertEqual(0, pool._basin.qsize())
        pool.fill()
        self.assertEqual(2, pool._current_number_of_objects)
        pool.put(obj_3)
        self.assertEqual(1, pool._current_number_of_objects)
        self.assertEqual(1, pool._basin.qsize())

    def test_default_wait_time(self):
        pool = Basin(connect, 1, default_wait=1).fill()
        obj_1 = pool.get()
        start_time = time()
        self.assertIsNone(pool.get(), ".get() did not return None, when None was expected")
        total_wait_time = time() - start_time
        self.assertAlmostEqual(1, total_wait_time, places=1)

    def test_basin_id(self):
        pool = Basin(connect, 1, disposer=disconnect)
        id_1 = pool._basin_id
        self.assertNotEqual(None, id_1, "The pool id was not updated")
        pool.fill()
        id_2 = pool._basin_id
        self.assertNotEqual(id_1, id_2, "The pool id was not updated")
        pool.empty()
        id_3 = pool._basin_id
        self.assertNotEqual(id_2, id_3, "The pool id was not updated")

    def test_basin_id_reset(self):
        pool = Basin(connect, 1)
        id = pool._basin_id
        pool._update_basin_id()
        self.assertNotEqual(id, pool._basin_id, "The pool ID was not updated as expected.")

    def test_start_refresh(self):
        pool = Basin(connect, 1, disposer=disconnect, refresh_seconds=1).fill()
        pid = pool._basin_id
        pool.stop_refresh()
        sleep(1.5)
        self.assertEqual(pid, pool._basin_id, "The stop_refresh did not stop the refresh from happening")
        pool.start_refresh()
        sleep(1.5)
        self.assertNotEqual(pid, pool._basin_id, "The start_refresh did not start the refreshing")
        pool.stop_refresh()

    def test_stop_refresh(self):
        pool = Basin(connect, 1, disposer=disconnect, refresh_seconds=1).fill()
        pid = pool._basin_id
        pool.stop_refresh()
        sleep(1.5)
        self.assertEqual(pid, pool._basin_id, "The stop_refresh did not stop the refresh from happening")

    def test_trigger_refresh_with_refresh_as_false(self):
        pool = Basin(connect, 1, disposer=disconnect, refresh_seconds=1).fill()
        pool.stop_refresh()
        self.assertFalse(pool.trigger_refresh())

    def test_strict_basin_size(self):
        pool = Basin(connect, 1, disposer=disconnect, max_size=2, grow=True, default_wait=1).fill()
        obj = pool.get()
        obj2 = pool.get()
        pool.trigger_refresh()
        self.assertIsNone(pool.get(False), "Grow did not stick to strick max size")
        pool.put(obj)
        self.assertEqual(pool._basin.qsize(), 1)
        pool.put(obj2)
        self.assertEqual(pool._basin.qsize(), 1, "The basin put grew past the min")

    def test_retry(self):
        count = RetryCount()
        pool = Basin(count.custom_retry_calc, 1, default_wait=False).fill()
        self.assertEqual(count.retries, 3, "Number of retries was {} and should be 3".format(str(count.retries)))

    def test_retry_back_off(self):
        retry_config = RetryConfig(retry=True, retry_wait_seconds=.5, back_off=False)
        count = RetryCount()
        pool = Basin(count.custom_retry_calc, 1, default_wait=False, retry_config=retry_config).fill()
        self.assertAlmostEqual(1.5, count.last_time - count.start_time, places=1)

    def test_retry_num_of_retries(self):
        retry_config = RetryConfig(retry=True, number_of_retries=2, retry_wait_seconds=.1)
        count = RetryCount()
        pool = Basin(count.custom_retry_calc, 1, default_wait=False, retry_config=retry_config).fill()
        self.assertEqual(count.retries, 2, "Number of retries was {} and should be 2".format(str(count.retries)))

    def test_retry_wait_seconds(self):
        retry_config = RetryConfig(retry=True, retry_wait_seconds=.5)
        count = RetryCount()
        pool = Basin(count.custom_retry_calc, 1, default_wait=False, retry_config=retry_config).fill()
        self.assertAlmostEqual(3, count.last_time - count.start_time, places=1)

    def test_retry_False(self):
        retry_config = RetryConfig(retry=False)
        count = RetryCount()
        pool = Basin(count.custom_retry_calc, 1, default_wait=False, retry_config=retry_config).fill()
        self.assertEqual(count.retries, 0, "Number of retries was {} and should be 0".format(str(count.retries)))

    def test_GenericError_with_create(self):
        retry_config = RetryConfig(retry=False)
        pool = Basin(connect_fail, 1, retry_config=retry_config, default_wait=False)
        self.assertRaises(BasinGenericError, lambda: pool._create())

    def test_GenericError_with_dispose(self):
        retry_config = RetryConfig(retry=False)
        pool = Basin(connect, 1, disposer=disconnect_fail, retry_config=retry_config, default_wait=False).fill()
        self.assertRaises(BasinGenericError, lambda: pool._dispose())

    def test_OutOfRangeError(self):
        retry_config = RetryConfig(retry=False)
        pool = Basin(connect, 1, retry_config=retry_config, default_wait=False).fill()
        self.assertRaises(BasinOutOfRange, lambda: pool._create())

    def test_PassThroughError(self):
        pool = Basin(connect_fail_pass_through, 1)
        self.assertRaises(ValueError, lambda: pool.fill())

    def test_get_block_param(self):
        pool = Basin(connect, 1, default_wait=.5).fill()
        pool.get()
        start_time = time()
        obj2 = pool.get()
        stop_time = time()
        self.assertAlmostEqual(.5, stop_time - start_time, places=2)
        self.assertIsNone(obj2, ".get() return and obj and it should not have done so")
        start_time = time()
        obj3 = pool.get(False)
        stop_time = time()
        self.assertAlmostEqual(0, stop_time - start_time, places=1)
        self.assertIsNone(obj3, ".get() return and obj and it should not have done so")


if __name__ == '__main__':
    unittest.main()