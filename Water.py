class Water:
    """
    Water is a dynamic object that merges an object into itself.  This is so any object can be used
    With birdbath.  Water contains 2 additional values that will be used to know when to refresh and which basin the
    object belongs to.  By creating an object like this, there is no functional difference between the object sent in
    and Water.
    """
    def __init__(self, obj):
        self.basin_expiration = None
        self.basin_id = None

        self.create_wine(obj)

    def create_wine(self, obj):
        """
        takes the object and loops over all of its attributes and adds them to the Water object.
        :param obj:  An object that the Basin creator function creats.
        :return: self
        """
        for attr in dir(obj):
            try:
                # since a class cannot absorb a __class__ method, this is filtered out
                if attr != "__class__":
                    setattr(self, attr, getattr(obj, attr))
            except TypeError as e:
                print "Caught error while turning water into wine"
                print e
        return self
