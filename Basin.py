from ErrorHandling import basin_purifier, BasinGenericError, BasinOutOfRange, RetryConfig
from Water import Water
from Queue import Queue, Empty
from datetime import datetime, timedelta
from time import sleep
import threading
import uuid


class Basin:
    """
    Basin has been created to be a self managing connection pool.  But for all practical purposes, it is essentially a
    pool manager for any object.  As of right now, Basin can only handle managing a pool of objects.
    This is due to some additional values being extended on to the object that is created by the "creator".
    At some point other Types could be added, but this will be on an as needed bases.
    """
    def __init__(self, creator, basin_size, disposer=None, refresh_seconds=None, grow=False, max_size=None,
                 default_wait=10, retry_config=None):
        """
        :param creator: function - The function that should be used to create the object that will be added to the
            basin. ex: connect to db.
            If params are needed, use a lambda ex: lambda: func(arg, arg)
            If no params are need just use the reference to the func.  Don't use parentheses
            * Consult the README for an example
        :param basin_size: int - size of queue Basin will try to maintain
        :param disposer: function - The function that should be used to disposes of the object that has been created.
            ex: close db connection.  Use this the same way as the creator
            * Consult the README for an example
        :param refresh_seconds: int - if this is set to anything greater than 0 it will activate the auto refresh functionality.
            Objects will be disposed of and created on the interval set in the value
        :param max_size: int - used if the grow is set to true.  This will allow more objects to be created, up to the max_size amount.
        :param grow: boolean - if True and max_size is set, new objects can be created if there is nothing in queue when .get() is called
        :param default_wait: int - how long the .get() will wait when trying to get an object out of the basin
        :param retry_config: RetryConfig object

        ----Additional values
        :var _basin_id: uuid - used as a version control for the basin
        :var _current_number_of_objects: int - keeps track of the current number of objects created.
            Incremented and decreased in _create() and _dispose()
        :var _refresh: boolean - tells the trigger_refresh and auto refresh function if should refresh or not
            Set via start_refresh() and stop_refresh()

        ----Notes
        Auto refreshing is only started by calling .fill() and .start_refresh()

        """
        self._min_size = basin_size
        self._max_size = max_size if max_size and max_size > basin_size else basin_size
        self._current_number_of_objects = 0

        self._basin = Queue(maxsize=self._max_size)
        self._basin_id = uuid.uuid4()
        self._default_wait = default_wait

        self._creator = self._verify_function_type(creator)
        self._disposer = self._verify_function_type(disposer) if disposer else None

        self._refresh_seconds = refresh_seconds
        self._refresh = True if refresh_seconds > 0 else False
        self._grow = grow

        # gets set below
        self._retry_config = None

        # forces required configs
        if self._refresh_seconds:
            if not self._disposer:
                raise ValueError("Using refresh_seconds requires the disposer option to be a valid function")
        if self._grow:
            if not self._disposer:
                raise ValueError("Using grow requires the disposer option to be a valid function")
        if retry_config and isinstance(retry_config, RetryConfig):
            self._retry_config = retry_config
        elif retry_config is None:
            self._retry_config = RetryConfig()
        else:
            raise ValueError("retry_config requires the use of the RetryConfig class")

    def fill(self, update_basin_id=True):
        """
        Fills the basin to the min size and prevents the number to go above the _max_size
        :param update_basin_id: boolean - the only reason this would be set to False is during a refresh, since refresh
        updates the basin_id.
        :return: self - for chaining purposes
        """
        # stopping and starting refresh in order to avoid a the possibility of an infinite loop that can happen if the
        # refresh_seconds is set too low
        refresh_status = self._refresh
        self.stop_refresh()
        if update_basin_id:
            basin_id = self._update_basin_id()
        # qsize is just what is the current size of of the queue.
        # current_number_of_objects refers to how many objs have been created
        while self._basin.qsize() < self._min_size and self._current_number_of_objects < self._max_size:
            try:
                obj = self._create()
            except BasinGenericError or BasinOutOfRange:
                break
            else:
                self.put(obj)
        # starts the refresh flag to True if it was True to begin with
        if refresh_status:
            self.start_refresh()
        return self

    def put(self, obj):
        """
        Puts the obj back in the basin if the proper requirements are met.
        :param obj: obj - must contain .basin_id and .basin_expiration
        :return: self
        """
        # if the basin has grown then don't put back in the basin if we don't need to
        if self._basin.qsize() >= self._min_size or obj.basin_id != self._basin_id:
            self._dispose(obj)
            return self
        # if basin_expiration has expired the connection will refresh
        if self._refresh and obj.basin_expiration and datetime.now() > obj.basin_expiration:
            try:
                obj = self._dispose(obj)._create()
            except BasinGenericError or BasinOutOfRange:
                pass
            else:
                self._basin.put(obj)
        else:
            self._basin.put(obj)
        return self

    def get(self, block=True):
        """
        Gets an obj from the basin.  The obj will be refreshed if the expiration has been been met.
        :return: obj
        """
        # implements the grow feature if needed
        if self._basin.qsize() == 0 and self._grow and self._current_number_of_objects < self._max_size:
            try:
                obj = self._create()  # returns None if unable to create a new obj
                # if None it will fall down to the _basin.get() which will wait for an available obj
                if obj is not None:
                    return obj
            except BasinOutOfRange:  # protection from a race condition
                pass
        try:
            obj = self._basin.get(block=block, timeout=self._default_wait)
        except Empty:
            return None
        if self._refresh and obj.basin_expiration and datetime.now() > obj.basin_expiration:
            try:
                obj = self._dispose(obj)._create()
            except BasinGenericError or BasinOutOfRange:  # protects against a race condition
                return self.get()  # if errors out while creating, it will just try again to hopefully get a valid obj

        return obj

    def set_disposer(self, disposer):
        """
        Gives the option to set the disposer after the Basin has been created
        :param disposer: function
        :return: self
        """
        self._disposer = self._verify_function_type(disposer)
        return self

    def empty(self):
        """
        This method is not used within the Basin class, it is here if needed for the consumer.
        :required disposer: The disposer is required to use this option.
        Empties the queue using the disposer.
        :return: self
        """
        if not self._disposer:
            raise ValueError("A disposer is required in order to empty a queue")
        basin_id = self._update_basin_id()  # this is done so incoming objs will not be put in the basin
        # loops over available obj in the queue and then disposes of them
        while True:
            obj = self.get(False)
            if not obj:
                break
            # Since basin is a FIFO queue the get should be pulling the oldest obj.  If the object has an older id it
            # will be disposed of.  If the id matches the current id, the assumption is new objects have been created
            # so the empty logic should finish up.
            if obj.basin_id == basin_id:
                self.put(obj)  # <- important
                break
            self._dispose(obj)
        return self

    def trigger_refresh(self):
        """
        If reset is set to False, via stop_refresh or it not being set, this function all not run.
        Refreshing is done by
        -updating the basin_id
        -get -> dispose -> create -> put over every obj present in the basin.
        -fill() in order to get the basin back to min_size

        fill() prevents the number of connections to go above the _max_size

        :required disposer: The disposer is required to use this option.
        :return: boolean
        """
        if not self._refresh:
            print "Unable to trigger the refresh, due to the refresh option being set to False. " \
                  "Use start_refresh() to start auto refreshing."
            return False
        if not self._disposer:
            raise ValueError("A disposer is required in order to refresh the queue")
        basin_id = self._update_basin_id()  # updates id so older objs are disposed of during the .put()
        while True:
            obj = self.get(False)
            if not obj:
                break
            # Since basin is a FIFO queue the get should be pulling the oldest obj.  If the object has an older id it
            # will be renewed.  If id matches, the assumption is we have reached the newly created objects.
            if obj.basin_id == self._basin_id:
                self.put(obj)  # <- important
                break
            self._dispose(obj)
            self.put(self._create())

        self.fill(update_basin_id=False)
        return True

    def stop_refresh(self):
        """
        This will set the refresh flag to False.  This will stop the auto refreshing from happening.
        :return: self
        """
        self._refresh = False
        return self

    def start_refresh(self):
        """
        This will set the refrsh flag to True.  This will allow the auto refresh to run.
        :return: True
        """
        self._refresh = True
        self._start_refresh_worker()
        return True

    @basin_purifier
    def _create(self):
        """
        Creates a new object by using the creator.  If _create is called and there are already too many connected, an
        OutOfRange error will be raised.  This should be caught by the calling function and handled properly.

        When an object is created additional values are added to it indicating the expiration and basin_id.

        _create is the only function that increases the _current_number_of_objects
        :return: obj / None if hit a BasinGenericError
        """
        if self._current_number_of_objects >= self._max_size:
            raise BasinOutOfRange()
        try:
            obj = Water(self._creator())
            obj.basin_expiration = datetime.now() + timedelta(0, self._refresh_seconds) if self._refresh_seconds else None
            obj.basin_id = self._basin_id
            self._current_number_of_objects += 1
            return obj
        except BasinGenericError:
            return None

    @basin_purifier
    def _dispose(self, obj):
        """
        Disposes and removes the object by using the disposer function.
        _disposer will ignore all errors that may be raised.

        _disposer is the only function that decreases the _current_number_of_objects
        :param obj: object - The object that was created by the creator
        :return:  self
        """
        try:
            self._disposer(obj)
        except BasinGenericError:
            pass
        self._current_number_of_objects -= 1
        return self

    def _update_basin_id(self):
        self._basin_id = uuid.uuid4()
        return self._basin_id

    def _start_refresh_worker(self):
        """
        Background thread that is sent the trigger_refresh function, so it can be triggered based off of the
        refresh_seconds param.  This will not run if self._refresh is set to False
        :return: boolean - True if it started, False if it was not started
        """
        if self._refresh and self._refresh_seconds <= 0:
            print "Unable to start auto refresh. Either refresh is set to False or refresh_seconds is <= to 0."
            return False

        def worker(refresh_func, sleep_time):
            sleep(sleep_time)
            refresh_func()

        t = threading.Thread(target=worker, args=(self.trigger_refresh, self._refresh_seconds))
        t.daemon = True  # True forces the thread to die when the main thread dies
        t.start()
        return True

    @staticmethod
    def _verify_function_type(function):
        if hasattr(function, "__call__"):
            return function
        else:
            raise ValueError("Expecting a valid function but received {}".format(type(function)))


if __name__ == "__main__":
    pass
