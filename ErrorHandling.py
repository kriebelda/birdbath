from sys import exc_info
from time import sleep

"""
# Custom Errors
"""


class BasinOutOfRange(Exception):
    def __init__(self):
        Exception.__init__(self, "The number of objs is out of range")


class BasinGenericError(IndexError):
    def __init__(self):
        Exception.__init__(self, "Generic Error for Basin")


class PerchGenericError(IndexError):
    def __init__(self):
        Exception.__init__(self, "Generic Error for Perch")


class BasinPassThroughError(Exception):
    def __init__(self, msg):
        Exception.__init__(self, "{}".format(str(msg)))
        self.e = exc_info()


class PerchPassThroughError(Exception):
    def __init__(self, msg):
        Exception.__init__(self, "{}".format(str(msg)))
        self.e = exc_info()

"""
Retry Logic
"""


class RetryConfig:
    def __init__(self, retry=True, number_of_retries=3, retry_wait_seconds=1,  back_off=True):
        """
        :param retry: boolean = if False there will be no retry on failures
        :param number_of_retries:  int - how many times it will retry before quitting
        :param retry_wait_seconds: int - how long there should be a sleep before retrying
        :param back_off: boolean - increments up the retry wait time as number_of_retries * retry_wait_seconds
        """
        self.retry = retry
        self.number_of_retries = number_of_retries
        self.back_off = back_off
        self.retry_wait_seconds = retry_wait_seconds


class Retry:
    def __init__(self, retry_config, func):
        self.config = retry_config
        self.attempts = 0
        self.func = func

    def start(self):
        while self.attempts < self.config.number_of_retries:
            self.attempts += 1
            sleep(self.config.retry_wait_seconds * (self.attempts if self.config.back_off else 1))
            try:
                return self.func()
            except:
                pass
        return None

"""
Decorator
"""


def basin_purifier(func):
    """
    Used as a decorator for error handling when calling the creator and disposer
    :param func: Function - the creator or disposer
    :return: Function - the wrapper
    """
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except BasinPassThroughError as e:
            # This will re-raise the actual error that caused the problem.
            e_info = e.e
            raise e_info[0], e_info[1], e_info[2]
        except BasinOutOfRange:
            raise BasinOutOfRange
        except:
            if self._retry_config.retry:
                result = Retry(self._retry_config, lambda: func(self, *args, **kwargs)).start()
            e = exc_info()
            print "Basin does not allow any contaminants in the water.  " \
                  "Please verify you are filtering out all pollutants before returning to our sparkling springs."\
                  "\ntype: {}\nmessage: {}".format(e[0],e[1])
            raise BasinGenericError
    return wrapper


def perch_purifier(func):
    """
    Used as a decorator for error handling when calling the creator and disposer
    :param func: Function - the creator or disposer
    :return: Function - the wrapper
    """
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except PerchPassThroughError as e:
            # This will re-raise the actual error that caused the problem.
            e_info = e.e
            raise e_info[0], e_info[1], e_info[2]
        except:
            if self._retry_config.retry:
                result = Retry(self._retry_config, lambda: func(self, *args, **kwargs)).start()
            e = exc_info()
            print "Perch only allows perfection when it comes to what is put on the stand.  " \
                  "Please verify you are filtering out all the imperfections before returning."\
                  "\ntype: {}\nmessage: {}".format(e[0],e[1])
            raise PerchGenericError
    return wrapper

