from ErrorHandling import PerchGenericError, RetryConfig, perch_purifier
from time import sleep
import threading


class Perch:
    def __init__(self, creator, disposer=None, refresh_seconds=None, retry_config=None, ignore_disposer=False):
        self._perch = None
        self._standing = False

        # basic config
        self._creator = self._verify_function_type(creator)
        self._disposer = self._verify_function_type(disposer) if disposer else None
        self._ignore_disposer = ignore_disposer

        # refresh config
        self._refresh_seconds = refresh_seconds
        self._refresh_status = True if refresh_seconds > 0 else False

        # gets set below
        self._retry_config = None

        # forces required configs
        if retry_config and isinstance(retry_config, RetryConfig):
            self._retry_config = retry_config
        elif retry_config is None:
            self._retry_config = RetryConfig()
        else:
            raise ValueError("retry_config requires the use of the RetryConfig class")

    def stand(self):
        """Creates the object and makes it available"""
        self._standing = True
        #  This refresh process should be started before the the first self._create() is called.  This will allow the
        #  refresh worker to get on the proper schedule.
        if self._refresh_seconds:
            if not self._ignore_disposer and not self._disposer:
                raise ValueError("Using refresh_seconds requires the disposer option to be a valid function")
            self.start_refresh()
        #  Creates the original obj to set on the perch.
        self._perch = self._create()
        return self

    def replace(self):
        """creates a new object and replaces what is on the perch.  Disposes the old object if available"""
        obj = self._create()
        old_obj = self._perch
        self._perch = obj
        if not self._ignore_disposer and self._disposer:
            self._dispose(old_obj)
        return self

    def refresh(self):
        """Differs from replace because it checks the _refresh var first"""
        if not self._refresh_status:
            print "Unable to trigger the refresh, due to the refresh option being set to False. " \
                  "Use start_refresh() to start auto refreshing."
            return False
        if not self._disposer and not self._ignore_disposer:
            raise ValueError("A disposer is required in order to refresh the queue")
        self.replace()
        return True

    def get(self):
        return self._perch

    def stop_refresh(self):
        """
        This will set the refresh flag to False.  This will stop the auto refreshing from happening.
        :return: self
        """
        self._refresh_status = False
        return self

    def start_refresh(self):
        """
        This will set the refresh flag to True.  This will allow the auto refresh to run.
        :return: True
        """
        self._refresh_status = True
        self._start_refresh_worker()
        return True

    def set_disposer(self, disposer):
        """
        Gives the option to set the disposer after the Perch has been created
        :param disposer: function
        :return: self
        """
        self._disposer = self._verify_function_type(disposer)
        return self

    @perch_purifier
    def _create(self):
        if not self._standing:
            raise Exception("The stand() method must be called before Perch can be used.")
        try:
            obj = self._creator()
            return obj
        except PerchGenericError:
            return None

    @perch_purifier
    def _dispose(self, obj):
        try:
            self._disposer(obj)
        except PerchGenericError:
            pass
        return self

    def _start_refresh_worker(self):
        """
        Background thread that is sent the trigger_refresh function, so it can be triggered based off of the
        refresh_seconds param.  This will not run if self._refresh_status is set to False
        :return: boolean - True if it started, False if it was not started
        """
        if not self._standing:
            raise Exception("The stand() method must be called before Perch can be used.")

        def replace_worker(refresh_func):
            """
            Worker thread that will run the refresh function sent in as an argument.  Catches all exceptions so the loop
            from refresh_sleep_worker is not broken.
            :param refresh_func: - function
            :return: boolean - basic boolean indicating completions of method. True if no exceptions. False if exception
            """
            try:
                refresh_func()
                return True
            except PerchGenericError:
                """Nothing needs to be done here because @perch_purifier will handle printing the error.  This just
                allows the refresh to continue as expected."""
                return False

        def refresh_sleep_worker(refresh_func, refresh_status, sleep_time, stand_status):
            """
            Thread created to sleep for the proper amount of time.  After the sleep time has been met, a new thread will
            be created to refresh the obj to set on the perch.
            :param refresh_func: function(reference) - function that will be called to create and replace the new object
            :param refresh_status: boolean - if the refresh option has been set properly
            :param sleep_time: int - length of time in seconds before the refresh_func should be called
            :param stand_status: boolean - validates if the stand() method has been called.  Will not run if False
            :return: boolean - basic boolean indicating completions of method
            """
            while True:
                sleep(sleep_time)
                if not stand_status and not refresh_status and sleep_time <= 0:
                    # if any of these conditions are met the loop should stop and the refresh worker should not be
                    # called.
                    break
                #  Starts the refresh/replace thread
                rt = threading.Thread(target=replace_worker, args=(refresh_func,))
                rt.daemon = True  # True forces the thread to die when the main thread dies
                rt.start()

        #  Starts the sleeper thread
        t = threading.Thread(target=refresh_sleep_worker,
                             args=(self.refresh, self._refresh_status, self._refresh_seconds, self._standing))
        t.daemon = True  # True forces the thread to die when the main thread dies
        t.start()
        return True

    @staticmethod
    def _verify_function_type(function):
        if hasattr(function, "__call__"):
            return function
        else:
            raise ValueError("Expecting a valid function but received {}".format(type(function)))
