Basin has been created to be a self managing connection pool.  But for all practical purposes, it is essentially a
pool manager for any object.  As of right now, Basin can only handle managing a pool of objects.
This is due to some additional values being extended on to the object that is created by the "creator".
At some point other types could be added, but this will be on an as needed bases.

Objects that can be created with BirdBath

* Basin: Pool that has additional options that includes refresh
* Perch: Object/Auth manager that hands out the requested data (ex: auth token) and can be continually refreshed
* RetryConfig: Object that can be used in conjunction with Basin and Perch to apply custom retry config when something fails during the object creation phase.

*Perch can also be used as a basic caching mechanism.*

*BirdBath handles errors, but can also throw certain errors if desired.*

```
---------------Options---------------
:param creator: function - what creates the object that will be added to the basin. ex: connect to db
    If params are needed, use a lambda ex: lambda: func(arg, arg)
    If no params are need just use the reference to the func.  Don't use parentheses
:param basin_size: int - size of queue Basin will try to maintain
:param disposer: function - The function that should be used to disposes of the object that has been created.
    ex: close db connection.  Use this the same way as the creator
:param refresh_seconds: int - if this is set to anything greater than 0 it will activate the auto refresh functionality.
    Objects will be disposed of and created on the interval set in the value
:param max_size: int - used if the grow is set to true.  This will allow more objects to be created, up to the max_size amount.
:param grow: boolean - if True and max_size is set, new objects can be created if there is nothing in queue when .get() is called
:param default_wait: int - how long the .get() will wait when trying to get an object out of the basin
:param retry_config: RetryConfig object

---------------Create Basin (Pool)---------------
**Example**
from birdbath import Basin, BasinPassThroughError
def connect():
    try:
        connection = connect(host=host_name, user=user_name, passwd=pw, db=db_name)
    except OperationalError as e:
        raise BasinPassThroughError("unable to log in you should bail out now.")

def disconnect(connection):
    connection.close()

try:
    pool = Basin(connect, 8, disposer=disconnect, default_wait=2, grow=True, max_size=3).fill()
except OperationalError as e:
    print e

---------------Error Handling---------------
Birdbath has a custom error that can be raised that will pass through the built in error handling.
When BasinPassThroughError is raised, it will pass throughall the error handling and raise the actual error, so
this can be caught when creating the pool. If this is not done, birdbath will not raise errors outside of it self, so the
basin will continue to be available.

from birdbath.ErrorHandling import BasinPassThroughError

**Example**
Used in the example for Create Basin


---------------Retry Config---------------

:param retry: boolean = if False there will be no retry on failures
:param number_of_retries:  int - how many times it will retry before quitting
:param retry_wait_seconds: int - how long there should be a sleep before retrying
:param back_off: boolean - increments up the retry wait time as number_of_retries * retry_wait_seconds

defaults:
retry=True
number_of_retries=3
retry_wait_seconds=1
back_off=True

**Example**
from libs.birdbath import Basin, BasinPassThroughError, RetryConfig
retry_config = RetryConfig(retry=False, number_of_retries=4, retry_wait_seconds=.5, back_off=True)
try:
    pool = Basin(hgh_connect, 1, refresh_seconds=1, disposer=hgh_disconnect, default_wait=2, grow=True, max_size=3, retry_config=retry_config).fill()
except OperationalError as e:
    print e
```